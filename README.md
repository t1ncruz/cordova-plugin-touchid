## Touch ID Plugin for iOS

Cordova Plugin to leverage the iOS local authentication framework to allow in-app user authentication using Touch ID.