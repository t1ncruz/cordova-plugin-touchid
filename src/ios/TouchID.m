//
//  TouchID.m
//  Copyright (c) 2014 Lee Crossley - http://ilee.co.uk
//

#import "TouchID.h"

#import <LocalAuthentication/LocalAuthentication.h>

@implementation TouchID

BOOL isFingerPrint = true;

- (void) authenticate:(CDVInvokedUrlCommand*)command;
{
    NSString *text = [command.arguments objectAtIndex:0];

    __block CDVPluginResult* pluginResult = nil;

    if (NSClassFromString(@"LAContext") != nil)
    {
        LAContext *laContext = [[LAContext alloc] init];
        NSError *authError = nil;
        
        if (@available(iOS 11.0, *)) {
            if (laContext.biometryType == LABiometryTypeFaceID) {
                isFingerPrint = false;
            }
        }
        
        laContext.localizedFallbackTitle = @"";

        if ([laContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
        {
            [laContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:text reply:^(BOOL success, NSError *error)
             {
                 if (success)
                 {
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                 }
                 else
                 {
                     NSString *errorCode = nil;
                     switch (error.code) {
                         case LAErrorAuthenticationFailed:
                             errorCode = @"authenticationFailed";
                             break;
                         case LAErrorUserCancel:
                             errorCode = @"userCancel";
                             break;
                         case LAErrorUserFallback:
                             errorCode = @"userFallback";
                             break;
                         case LAErrorSystemCancel:
                             errorCode = @"systemCancel";
                             break;
                         case LAErrorPasscodeNotSet:
                             errorCode = @"passcodeNotSet";
                             break;
                         case LAErrorTouchIDNotAvailable:
                             errorCode = @"touchIDNotAvailable";
                             break;
                         case LAErrorTouchIDNotEnrolled:
                             errorCode = @"touchIDNotEnrolled";
                             break;
                         default:
                             errorCode = @"unknown";
                             break;
                     }
                     
                     if ((long)error.code == -8 || (long)error.code == -1) {
                         [self displayAlertView];
                     }
                     
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorCode];
                 }
                 
                 [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
             }];
        }
        else
        {
            if ([authError code] == -8 || [authError code] == -1) {
                [self displayAlertView];
            }
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[authError localizedDescription]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)displayAlertView{
    NSString *stringAlertTitle = @"El número de intentos llegó al límite";
    NSString *stringAlertMessage = @"Para ingresar digita el usuario y la clave";
    NSString *stringIconImage = isFingerPrint ? @"fingerprint-icon.png" : @"faceid.png";
    UIAlertController *alertWithImage = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"\n\n%@",stringAlertTitle]
                                                                            message:stringAlertMessage
                                                                     preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *firstAlertAction = [UIAlertAction actionWithTitle:@"Cancelar"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [alertWithImage dismissViewControllerAnimated:YES completion:nil];
                                                             }];
    [alertWithImage addAction:firstAlertAction];
    UIImage *image = [UIImage imageNamed:stringIconImage];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((270 - 45)/2, 15, 45, 45)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alertWithImage.view addSubview:imageView];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    if (viewController != nil) {
        [viewController presentViewController:alertWithImage animated:YES completion:nil];
    }
}

- (void) checkSupport:(CDVInvokedUrlCommand*)command;
{

    __block CDVPluginResult* pluginResult = nil;

    if (NSClassFromString(@"LAContext") != nil)
    {
        LAContext *laContext = [[LAContext alloc] init];
        NSError *authError = nil;

        if ([laContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
        {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        else
        {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[authError localizedDescription]];
        }
    }
    else
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
